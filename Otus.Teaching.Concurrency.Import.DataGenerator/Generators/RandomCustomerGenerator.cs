using System.Collections.Generic;
using Bogus;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.DataGenerator
{
    public static class RandomCustomerGenerator
    {
        public static Customer GenerateById(int _id)
        {
            var customersFaker = new Faker<Customer>()
               .CustomInstantiator(f => new Customer()
               {
                   id = _id
               })
               .RuleFor(u => u.fullName, (f, u) => f.Name.FullName())
               .RuleFor(u => u.email, (f, u) => f.Internet.Email(u.fullName))
               .RuleFor(u => u.phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker.Generate();
        }
        public static List<Customer> Generate(int dataCount)
        {
            var customers = new List<Customer>();
            var customersFaker = CreateFaker();

            foreach (var customer in customersFaker.GenerateForever())
            {
                customers.Add(customer);

                if (dataCount == customer.id)
                    return customers;
            }

            return customers;
        }

        private static Faker<Customer> CreateFaker()
        {
            var id = 1;
            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(f => new Customer()
                {
                    id = id++
                })
                .RuleFor(u => u.fullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.email, (f, u) => f.Internet.Email(u.fullName))
                .RuleFor(u => u.phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
    }
}