using System.Text.Json;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.DataGenerator
{
    public class JsonGenerator : IDataGenerator
    {
        private readonly int id;
        public string CustomerJson { get; set; }
        public JsonGenerator(int _id)
        {
            id = _id;
        }

        public void Generate()
        {
            var customer = RandomCustomerGenerator.GenerateById(id);
            CustomerJson = JsonSerializer.Serialize(customer);
        }
    }
}