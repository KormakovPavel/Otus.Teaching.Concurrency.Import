﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.WebAPI
{
    [Route("[controller]")]
    public class UsersController : Controller
    {
        private readonly ICustomerRepository repository;
        public UsersController(ICustomerRepository _repository)
        {
            repository = _repository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {           
            var customer = await repository.GetCustomerById(id);

            if (customer == null)
            {
                return NotFound();               
            }
            
            return Ok(customer);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Customer customer)
        {
            var customerInBD = await repository.GetCustomerById(customer.id);
                        
            if (customerInBD == null)
            {
                await repository.AddCustomerAsync(customer);
                return Ok();
            }
            
            return Conflict();
        }
    }
}
