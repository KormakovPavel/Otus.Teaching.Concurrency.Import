﻿using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataGenerator;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.WebClient
{
    class Program
    {
        static async Task Main()
        {
            var configParams = JsonSerializer.Deserialize<ConfigParams>(File.ReadAllText("appsettings.json"));

            Console.Write("Введите ID пользователя для поиска: ");
            int.TryParse(Console.ReadLine(), out int idFind);

            var client = new HttpClient();
            try
            {
                var resultFind = await client.GetAsync($"{configParams.WebServer}users/{idFind}");
                Console.WriteLine($"Статус код: {resultFind.StatusCode}");

                var content = await resultFind.Content.ReadAsStringAsync();
                var customer = JsonSerializer.Deserialize<Customer>(content);
                Console.WriteLine($"Данные пользователя:\n{customer.GetDataToString()}");
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine($"Ошибка: {ex.Message}");
            }

            Console.Write("Введите ID пользователя для добавления: ");
            int.TryParse(Console.ReadLine(), out int idAdd);
            var jsonGenerator = new JsonGenerator(idAdd);
            jsonGenerator.Generate();
            var contentAdd = new StringContent(jsonGenerator.CustomerJson, Encoding.UTF8, "application/json");
            try
            {
                var resultAdd = await client.PostAsync($"{configParams.WebServer}users", contentAdd);
                Console.WriteLine($"Статус код: {resultAdd.StatusCode}");
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine($"Ошибка: {ex.Message}");
            }

            Console.ReadLine();
        }
    }
}
