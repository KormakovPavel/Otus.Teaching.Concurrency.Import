﻿using Otus.Teaching.Concurrency.Import.Core;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class XmlParser
        : IDataParser<XElement, Customer>
    {
        public Customer Parse(XElement row)
        {
            return new Customer
            {
                id = int.Parse(row.Element("id").Value),
                fullName = row.Element("fullName").Value,
                email = row.Element("email").Value,
                phone = row.Element("phone").Value
            };
        }
    }
}