﻿using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class CsvParser
        : IDataParser<string, Customer>
    {
        public Customer Parse(string row)
        {
            var parts = row.Split(';');

            return new Customer
            {
                id = int.Parse(parts[0]),
                fullName = parts[1],
                email = parts[2],
                phone = parts[3]
            };
        }
    }
}