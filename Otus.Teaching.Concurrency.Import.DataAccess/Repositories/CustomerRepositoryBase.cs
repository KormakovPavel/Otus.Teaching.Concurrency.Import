using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public abstract class CustomerRepositoryBase
        : ICustomerRepository
    {
        protected readonly DataContext dataContext;
        private readonly int attemptAll;
        public CustomerRepositoryBase(DataContext _dataContext, int _attemptAll = 5)
        {
            dataContext = _dataContext;
            attemptAll = _attemptAll;
        }
        public abstract Task<Customer> GetCustomerById(int id);

        public void AddCustomers(List<Customer> customers)
        {
            dataContext.Customers.AddRange(customers);
            SaveInDatabase();
        }

        public async Task AddCustomerAsync(Customer customer)
        {
            await dataContext.Customers.AddAsync(customer);
            await SaveInDatabaseAsync();
        }

        public async Task AddCustomersAsync(List<Customer> customers)
        {
            await dataContext.Customers.AddRangeAsync(customers);
            await SaveInDatabaseAsync();
        }

        private void SaveInDatabase(int attempt = 1)
        {
            if (attempt < attemptAll + 1)
            {
                Console.WriteLine($"{attempt} ������� ������ � ��");
                attempt++;
                try
                {
                    dataContext.SaveChanges();
                }
                catch (Microsoft.Data.Sqlite.SqliteException ex)
                {
                    Console.WriteLine(ex.Message);
                    SaveInDatabase(attempt);
                }
            }
        }

        private async Task SaveInDatabaseAsync(int attempt = 1)
        {
            if (attempt < attemptAll + 1)
            {
                Console.WriteLine($"{attempt} ������� ������ � ��");
                attempt++;
                try
                {
                    await dataContext.SaveChangesAsync();
                }
                catch (Microsoft.Data.Sqlite.SqliteException ex)
                {
                    Console.WriteLine(ex.Message);
                    await SaveInDatabaseAsync(attempt);
                }
            }
        }


    }
}