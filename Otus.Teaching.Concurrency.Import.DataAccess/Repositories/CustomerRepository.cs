using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class CustomerRepository
        : CustomerRepositoryBase, IDisposable
    {
        public CustomerRepository(DataContext dataContext, int attemptAll = 5) : base(dataContext, attemptAll)
        {

        }

        public override async Task<Customer> GetCustomerById(int id)
        {
            return await dataContext.Customers.FirstOrDefaultAsync(w => w.id == id);
        }

        public void Dispose()
        {
            dataContext.Dispose();
        }

    }
}