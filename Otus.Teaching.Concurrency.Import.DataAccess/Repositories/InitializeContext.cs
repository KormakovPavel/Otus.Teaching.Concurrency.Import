﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public static class InitializeContext
    {
        public static DataContext GetContext(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();

            var options = optionsBuilder
                    .UseSqlite(connectionString)
                    .Options;

            return new DataContext(options);
        }
    }
}
