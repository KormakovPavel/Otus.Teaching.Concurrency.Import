﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}