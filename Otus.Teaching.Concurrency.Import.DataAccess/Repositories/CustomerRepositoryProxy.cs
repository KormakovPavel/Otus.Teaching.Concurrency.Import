using System;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Core;
using Microsoft.Extensions.Caching.Memory;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class CustomerRepositoryProxy
        : CustomerRepositoryBase, IDisposable
    {
        private readonly IMemoryCache cache;
        private CustomerRepository customerRepository;
        public CustomerRepositoryProxy(DataContext dataContext, IMemoryCache _cache) : base(dataContext)
        {
            cache = _cache;
        }
        public override async Task<Customer> GetCustomerById(int id)
        {
            if (!cache.TryGetValue(id, out Customer customer))
            {
                if (customerRepository == null)
                {
                    customerRepository = new CustomerRepository(dataContext);
                }

                customer = await customerRepository.GetCustomerById(id);
                if (customer != null)
                {
                    cache.Set(customer.id, customer, new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(1)));
                }
            }
            return customer;
        }
        public void Dispose()
        {
            if (customerRepository != null)
            {
                customerRepository.Dispose();
            }
        }
    }
}