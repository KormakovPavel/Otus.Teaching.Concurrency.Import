using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface ICustomerRepository
    {
        void AddCustomers(List<Customer> customer);
        Task AddCustomerAsync(Customer customer);
        Task<Customer> GetCustomerById(int id);
    }
}