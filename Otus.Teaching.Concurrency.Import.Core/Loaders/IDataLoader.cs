﻿using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IDataLoader<T>
    {        
        IEnumerable<T> LoadData(int beginId, int endId);
    }
}