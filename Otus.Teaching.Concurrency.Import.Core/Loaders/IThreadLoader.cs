﻿
namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IThreadLoader
    {
        void LoadData();
    }
}