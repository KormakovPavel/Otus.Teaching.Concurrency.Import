﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public class ConfigParams
    {
        public string WebServer { get; set; }
        public string ConnectionString { get; set; }
        public string PathGenerator { get; set; }
        public string FileGenerator { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public int CountRows { get; set; }
        public int CountThreads { get; set; }
        public int AttemptAll { get; set; }
        public bool IsPool { get; set; }
        public string FullPathFileName
        {
            get
            {
                return Path.Combine(PathGenerator, $"{FileName}.{Extension}");
            }
        }
        public string FullFileName
        {
            get
            {
                return $"{FileName}.{Extension}";
            }
        }
        public string FullPathGenerator
        {
            get
            {
                return Path.Combine(PathGenerator, FileGenerator);
            }
        }
    }
}
