using Newtonsoft.Json;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public class Customer
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("fullName")]
        public string fullName { get; set; }
        [JsonProperty("email")]
        public string email { get; set; }
        [JsonProperty("phone")]
        public string phone { get; set; }
        public Customer()
        {
            id = 0;
            fullName = "";
            email = "";
            phone = "";
        }

        public override string ToString()
        {
            return $"{id};{fullName};{email};{phone}";
        }

        public string GetDataToString()
        {
            return $"ID: {id}\n���: {fullName}\nEmail: {email}\n�������: {phone}";
        }
    }
}