﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IDataParser<T,V>
    {
        V Parse(T row);
    }
}