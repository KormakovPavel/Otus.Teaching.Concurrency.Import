using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IDataGenerator
    {
        void Generate();

        public async Task GenerateAsync()
        {
            await Task.Run(() => Generate());
        }
    }
}