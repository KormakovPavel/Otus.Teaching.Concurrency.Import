﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Otus.Teaching.Concurrency.Import.WebAPI;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Core;
using System.Text.Json;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.Concurrency.Import.Tests
{
    public class UsersControllerTests
    {
        private readonly UsersController usersController;
        public UsersControllerTests()
        {
            var configParams = JsonSerializer.Deserialize<ConfigParams>(File.ReadAllText("appsettings.json"));
            var dataContext = InitializeContext.GetContext(configParams.ConnectionString);
            var repository = new CustomerRepository(dataContext);
            usersController = new UsersController(repository);
        }
        [Fact]
        public async Task GetCustomerByIdWithStatusCodeOk()
        {           
            var result =  await usersController.Get(2);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetCustomerByIdWithStatusCodeNotFound()
        {            
            var result = await usersController.Get(0);
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task AddCustomerWithStatusCodeConflict()
        {
            var customer = new Customer()
            {
                id = 2,
                phone = "79141234567",
                fullName = "John Doe",
                email = "doe1988@gmail.com"
            };

            var result = await usersController.Post(customer);
            Assert.IsType<ConflictResult>(result);
        }

        [Fact]
        public async Task AddCustomerWithStatusCodeOK()
        {
            var customer = new Customer()
            {
                id = 10000200,
                phone = "79141234567",
                fullName = "John Doe",
                email = "doe1988@gmail.com"
            };

            var result = await usersController.Post(customer);
            Assert.IsType<OkResult>(result);
        }
    }
}
