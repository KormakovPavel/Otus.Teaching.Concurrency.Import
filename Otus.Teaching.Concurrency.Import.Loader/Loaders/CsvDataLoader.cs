﻿using System.Linq;
using System.IO;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class CsvDataLoader : IDataLoader<string>
    {
        private readonly IEnumerable<string> rows;
        public CsvDataLoader(string fileName)
        {
            rows = File.ReadAllLines(fileName);
        }

        public IEnumerable<string> LoadData(int beginId, int endId)
        {
            return rows.ToArray()[beginId..endId];
        }
    }
}
