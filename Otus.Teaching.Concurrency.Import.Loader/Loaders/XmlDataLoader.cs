﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class XmlDataLoader : IDataLoader<XElement>
    {
        private readonly IEnumerable<XElement> rows;
        public XmlDataLoader(string fileName)
        {
            var xdoc = XDocument.Load(fileName);
            rows = xdoc.Element("Customers").Element("Customers").Elements("Customer");
        }

        public IEnumerable<XElement> LoadData(int beginId, int endId)
        {
            return rows.Where(w => int.Parse(w.Element("id").Value) >= beginId && int.Parse(w.Element("id").Value) <= endId);
        }
    }
}
