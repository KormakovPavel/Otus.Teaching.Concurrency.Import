﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class LoderThreads<T> : IThreadLoader
    {
        private readonly List<Thread> queueThreads = new List<Thread>();
        private readonly IDataParser<T, Customer> parser;
        private readonly IDataLoader<T> loader;
        private readonly int countThreads;
        private readonly int countRows;
        private readonly int attemptAll;
        private readonly string connectionString;
        public LoderThreads(IDataLoader<T> _loader, IDataParser<T, Customer> _parser, int _countThreads, int _countRows, int _attemptAll, string _connectionString)
        {
            loader = _loader;
            parser = _parser;
            countThreads = _countThreads;
            countRows = _countRows;
            attemptAll = _attemptAll;
            connectionString = _connectionString;
        }
        public void LoadData()
        {
            Console.WriteLine("Загрузка данных через одиночные потоки...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            int stepRange = countRows / countThreads;
            int beginId = 1;
            int endId = stepRange;

            for (int i = 0; i < countThreads; i++)
            {
                var facadeLoader = new FacadeLoader<T>(loader, parser, beginId, endId, attemptAll, connectionString);
                queueThreads.Add(StartThread(facadeLoader));

                beginId += stepRange;
                endId += stepRange;
            }

            while (true)
            {
                int countStopThreads = 0;
                foreach (var currentThread in queueThreads)
                {
                    if (currentThread.ThreadState == System.Threading.ThreadState.Stopped)
                    {
                        countStopThreads++;
                    }
                }

                if (countStopThreads == countThreads)
                {
                    stopWatch.Stop();
                    Console.WriteLine($"Загрузка завершена за {stopWatch.Elapsed}");
                    break;
                }
            }
        }

        private Thread StartThread(FacadeLoader<T> facadeLoader)
        {
            var thread = new Thread(new ParameterizedThreadStart(facadeLoader.LoadData));
            thread.Start(new object());

            return thread;
        }
    }
}
