﻿using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class ThreadPoolItem<T>
    {
        public FacadeLoader<T> FacadeLoader { get; private set; }

        public WaitHandle WaitHandle { get; private set; }

        public ThreadPoolItem(FacadeLoader<T> _facadeLoader)
        {
            FacadeLoader = _facadeLoader;
            WaitHandle = new AutoResetEvent(false);
        }
    }
}