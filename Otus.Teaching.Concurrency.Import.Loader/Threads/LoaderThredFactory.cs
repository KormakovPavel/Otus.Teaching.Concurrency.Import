using System.IO;
using System.Xml.Linq;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class LoaderThredFactory
    {
        private readonly string fileName;
        private readonly int countThreads;
        private readonly int countRows;
        private readonly int attemptAll;
        private readonly string connectionString;
        private readonly bool isPoll;
        public LoaderThredFactory(string _fileName, int _countThreads, int _countRows, int _attemptAll, string _connectionString, bool _isPoll = false)
        {
            fileName = _fileName;
            countThreads = _countThreads;
            countRows = _countRows;
            attemptAll = _attemptAll;
            connectionString = _connectionString;
            isPoll = _isPoll;
        }
        public IThreadLoader GetLoaderThred()
        {
            return GetExtension(fileName) switch
            {
                ".xml" => XmlLoader(),
                ".csv" => CsvLoader(),
                _ => null
            };
        }
        private string GetExtension(string fileName)
        {
            var fileInfo = new FileInfo(fileName);
            return fileInfo.Extension;
        }
        private IThreadLoader XmlLoader()
        {
            var loader = new XmlDataLoader(fileName);
            var parser = new XmlParser();

            if (isPoll)
                return new LoderThreadPool<XElement>(loader, parser, countThreads, countRows, attemptAll, connectionString);
            else
                return new LoderThreads<XElement>(loader, parser, countThreads, countRows, attemptAll, connectionString);
        }
        private IThreadLoader CsvLoader()
        {
            var loader = new CsvDataLoader(fileName);
            var parser = new CsvParser();

            if (isPoll)
                return new LoderThreadPool<string>(loader, parser, countThreads, countRows, attemptAll, connectionString);
            else
                return new LoderThreads<string>(loader, parser, countThreads, countRows, attemptAll, connectionString);
        }
    }
}