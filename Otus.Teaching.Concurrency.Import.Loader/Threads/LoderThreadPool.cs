﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class LoderThreadPool<T> : IThreadLoader
    {
        private readonly List<ThreadPoolItem<T>> queueThreads = new List<ThreadPoolItem<T>>();
        private readonly IDataParser<T, Customer> parser;
        private readonly IDataLoader<T> loader;
        private readonly int countThreads;
        private readonly int countRows;
        private readonly int attemptAll;
        private readonly string connectionString;
        public LoderThreadPool(IDataLoader<T> _loader, IDataParser<T, Customer> _parser, int _countThreads, int _countRows, int _attemptAll, string _connectionString)
        {
            loader = _loader;
            parser = _parser;
            countThreads = _countThreads;
            countRows = _countRows;
            attemptAll = _attemptAll;
            connectionString = _connectionString;
        }
        public void LoadData()
        {
            Console.WriteLine("Загрузка данных через пул потоков...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            int stepRange = countRows / countThreads;
            int beginId = 1;
            int endId = stepRange;
            for (int i = 0; i < countThreads; i++)
            {
                var facadeLoader = new FacadeLoader<T>(loader, parser, beginId, endId, attemptAll, connectionString);
                var threadPoolItem = new ThreadPoolItem<T>(facadeLoader);
                queueThreads.Add(threadPoolItem);
                ThreadPool.QueueUserWorkItem(LoaderInThreadPool, threadPoolItem);

                beginId += stepRange;
                endId += stepRange;
            }

            WaitHandle[] waitHandles = queueThreads.Select(x => x.WaitHandle).ToArray();
            WaitHandle.WaitAll(waitHandles);

            stopWatch.Stop();
            Console.WriteLine($"Загрузка завершена за {stopWatch.Elapsed}");
        }
        private void LoaderInThreadPool(object item)
        {
            var schedulerItem = (ThreadPoolItem<T>)item;
            schedulerItem.FacadeLoader.LoadData(new object());
            var autoResetEvent = (AutoResetEvent)schedulerItem.WaitHandle;
            autoResetEvent.Set();
        }
    }
}
