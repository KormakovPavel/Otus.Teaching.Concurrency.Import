using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class FacadeLoader<T>
    {
        private readonly IDataParser<T, Customer> parser;
        private readonly IDataLoader<T> loader;
        private readonly int beginId;
        private readonly int endId;
        private readonly int attemptAll;
        private readonly string connectionString;
        public List<Customer> Customers { get; set; }
        public FacadeLoader(IDataLoader<T> _loader, IDataParser<T, Customer> _parser, int _beginId, int _endId, int _attemptAll, string _connectionString)
        {
            Customers = new List<Customer>();
            loader = _loader;
            parser = _parser;
            beginId = _beginId;
            endId = _endId;
            attemptAll = _attemptAll;
            connectionString = _connectionString;
        }
        public void LoadData(object stateObject)
        {
            Console.WriteLine($"������ ��������� ��������� Id: {beginId}-{endId}");

            var rows = loader.LoadData(beginId, endId);

            foreach (var row in rows)
            {
                var customer = parser.Parse(row);
                Customers.Add(customer);
            }

            using var dataContex = InitializeContext.GetContext(connectionString);
            using var customerRepository = new CustomerRepository(dataContex, attemptAll);
            customerRepository.AddCustomers(Customers);

            Console.WriteLine($"����� ��������� ��������� Id: {beginId}-{endId}");
        }       
    }
}