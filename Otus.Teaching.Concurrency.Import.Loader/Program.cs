﻿using System;
using System.Xml.Linq;
using System.Linq;
using System.Text.Json;
using System.Diagnostics;
using System.IO;
using DataGeneratorApp = Otus.Teaching.Concurrency.Import.DataGenerator.App;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.DataAccess;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static ConfigParams configParams;
        static void Main(string[] args)
        {
            configParams = JsonSerializer.Deserialize<ConfigParams>(File.ReadAllText("appsettings.json"));

            InitilazeDataBase();

            var fileName = GenerateCustomersDataFile(args);

            var loaderThredFactory = new LoaderThredFactory(fileName, configParams.CountThreads, configParams.CountRows, configParams.AttemptAll, configParams.ConnectionString, configParams.IsPool);
            var loaderThred = loaderThredFactory.GetLoaderThred();
            loaderThred.LoadData();

            Console.ReadLine();
        }

        static string GenerateCustomersDataFile(string[] args)
        {
            string fileName = "";

            if (args == null || !args.Any())
            {
                throw new ArgumentException("Отсутствуют аргументы");
            }

            if (configParams == null)
            {
                throw new ArgumentException("Конфигурация не инициализирована");
            }

            if (args.Length == 1)
            {
                switch (args[0])
                {
                    case "-m":
                        fileName = configParams.FullFileName;
                        DataGeneratorApp.Program.Main(new string[] { fileName, $"{configParams.CountRows}" });
                        break;
                    case "-p":
                        fileName = configParams.FullPathFileName;
                        Process.Start(Path.Combine(configParams.PathGenerator, configParams.FileGenerator), $"{fileName} {configParams.CountRows}");
                        break;
                }

            }
            else
            {
                throw new ArgumentException("Required right command line arguments");
            }

            return fileName;
        }

        private static void InitilazeDataBase()
        {
            var dataContext = InitializeContext.GetContext(configParams.ConnectionString);
            dataContext.Database.EnsureDeleted();
            dataContext.Database.EnsureCreated();
        }
    }
}