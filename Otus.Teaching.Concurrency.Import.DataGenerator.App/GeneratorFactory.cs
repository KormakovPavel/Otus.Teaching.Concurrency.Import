using System.IO;
using Otus.Teaching.Concurrency.Import.Core;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.CsvGenerator;


namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount)
        {
            return GetExtension(fileName) switch
            {
                ".xml" => new XmlDataGenerator(fileName, dataCount),
                ".csv" => new CsvDataGenerator(fileName, dataCount),
                _ => null
            };
        }

        private static string GetExtension(string fileName)
        {
            var fileInfo = new FileInfo(fileName);
            return fileInfo.Extension;
        }
    }
}