﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName;
        private static int _dataCount = 100;

        public static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
            {
                return;
            }

            Console.WriteLine($"Генерация данных...");

            var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount);

            if (generator != null)
            {
                Task.WaitAll(generator.GenerateAsync());

                Console.WriteLine($"Данные сгенерированы в файл: {_dataFileName}");
            }
            else
            {
                throw new ArgumentException("Ошибка генерации. Расширение не определенно");
            }
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }

            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }

            return true;
        }


    }
}